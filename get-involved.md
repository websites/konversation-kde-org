---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# Get Involved!

Want to make Konversation better? Consider getting involved in Konversation development
and help us make Konversation the best irc client!

## Build Konversation from Source

The [community wiki](https://community.kde.org/Get_Involved/development)
provides excellent resources for setting up your very own development
environment.

## Get in Touch!

Most development-related discussions take place on the [konversation-devel mailing
list](http://mail.kde.org/mailman/listinfo/konversation-devel). Just join in, say hi
and tell us what you would like to help us with! You can also chat with us on [#konversation](irc://irc.libera.chat/konversation)
on Libera Chat.

## Not a Programmer?

Not a problem! There's plenty of other tasks that you can help us with to
make Konversation better, even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  mis-filed, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help to translate
  Konversation into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user
  documentation to make Konversation more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote Konversation
  both online and offline
* [Updating wiki](https://userbase.kde.org/Konversation) - help update the information present in
  the wiki, add new tutorials, etc. - help make it easier for others to join!
* Do you have any other idea? Get in touch!

