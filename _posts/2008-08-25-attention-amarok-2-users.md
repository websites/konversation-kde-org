---
title: Attention Amarok 2 users
date: 2008-08-25
layout: post
---
Those wishing to query their Amarok 2 music player [Beta 1 released](http://amarok.kde.org/en/releases/2.0/beta/1) from within Konversation using `/media` or `/audio` will be interested to learn that the 'media' script shipped with Konversation 1.1 does in fact contain experimental Amarok 2 support, albeit disabled due to being merged during feature freeze.


If you're familiar with [Python](http://www.python.org/Python) coding, you may simply want to [edit](http://konversation.kde.org/wiki/FAQ#Where_can_I_find_the_scripts_included_with_Konversation_.28.2Fmedia.2C_.2Fweather.2C_etc..29.3F) your existing copy yourself. Alternatively, [grab](http://websvn.kde.org/*checkout*/branches/extragear/kde3/network/konversation/scripts/media) the latest version (with Amarok 2 support enabled by default) from SVN, move it to `~/.kde/share/apps/konversation/scripts/` and mark it as executable to override the system-installed version.

To those in the know, please note that the implementation is presently Amarok 2-specific; a generic MPRIS client implementation for the 'media' script is planned to be included with the initial KDE 4 release of Konversation (and yes, we're making progress on that).

