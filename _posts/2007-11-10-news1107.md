---
title: An Update on Konversation and KDE 4 
date: 2007-11-10
layout: post
---
Many of you have been wondering what our plans with regard to KDE 4 are, and since you most certainly deserve a good answer on that one, here's our little official statement. First, let's get the important bits out of the way: Yes, **Konversation will be ported to KDE 4**. 

 That said, what we're currently working on is a last release for KDE 3.5, Konversation 1.1. Why work on KDE 3 at this stage? There are a number of reasons: 

 
As the current release cycle on the KDE 3 codebase is nearing its end, we feel it's the right call to deliver the significant improvements that have been made to you now, rather than make you wait longer for the additional time we'd need to finish the KDE 4 port. 

2008 will see a number of significant distribution releases which will still base their default desktop on KDE 3.5, such as Kubuntu 8.04 ("Hardy Heron"). We feel that the mature KDE 3 codebase in the form of the updated and improved Konversation 1.1 will be a perfect fit for these mature, reliable KDE 3.5 desktops. 

We think that a KDE 3-based version of Konversation will provide the most value to you in this timeframe, as implementing a version of Konversation that achieves feature parity with Konversation 1.0.1 (or 1.1) is not possible on top of KDE 4.0. As it stands, KDE 4.0 will lack a secure socket API (enabling SSL support integrated with KDE's certificate management) and an equivalent of the API we currently use to provide integration with KAddressBook. Both will materialize in due time, but are not available to us now. 
 

 What's the schedule then, you ask? We currently expect to close the book on Konversation 1.1 in the near future, and deliver it to you lovingly shrink-wrapped in a fetching tarball soon after. Meanwhile, preliminary work on the KDE 4 port has started in the form of prototyping a while ago, and it will switch into high gears when Konversation 1.1 goes into string freeze (the period in which the feature complete codebase undergoes final localization updates for the release). 

