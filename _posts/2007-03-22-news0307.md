---
title: Assorted news
date: 2007-03-22
layout: post
---
 
+ The developers of [pisg](http://pisg.sourceforge.net/), the popular IRC log analysis and statistics generator toolkit written in Perl, have recently added support for Konversation's log file format. Available in their CVS repository it is destined for wider circulation as part of their next release. Thanks to Torbjörn Svensson of the pisg team! 
+ Starting with version 0.2.15, the desktop search application [Beagle](http://beagle-project.org/Main_Page)(KDE frontends [here](http://kde-apps.org/content/show.php?content=36832)[here](http://kde-apps.org/content/show.php?content=28437) and [here](http://kde-apps.org/content/show.php?content=35781) features a plugin capable of extracting and indexing conversations from Konversation's log files. Thanks to Debajyoti Bera of the Beagle team for writing it! 
+ And finally, Kubuntu users are urged to upgrade to the upcoming Kubuntu release 7.04 Feisty Fawn, scheduled for April of this year, at their earliest convenience, due to bugs in the Konversation packaging for Kubuntu 6.10 Edgy Eft that break a number of menu structures and application behaviors. The same may apply to some Kubuntu-derived distributions. 
 


