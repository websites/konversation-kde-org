---
title: Konversation has switched to Git
date: 2009-12-10
layout: post
---

As many of you probably already know, KDE has its eyes set on [moving to Git](http://techbase.kde.org/Projects/MovetoGit), the revision control system developed by the Linux kernel developers in 2005 (and now in use at a [great many places](http://en.wikipedia.org/wiki/Git_%28software%29#Projects_using_Git)). Everyone's favorite music player Amarok already [made the switch](http://blog.lydiapintscher.de/2009/07/20/were-testing-the-water-for-everyone/) in July of this year, and today Konversation has followed suit! The nitty-gritty follows, Q&amp;A style:

**Why Git?**

Most Konversation developers have used Git for some time already, either on other projects or even for their Konversation work (via [git-svn](http://www.kernel.org/pub/software/scm/git/docs/git-svn.html)), and have developed a strong preference for it, especially due to the superior performance and the way it greatly improves the local workflow by adding powerful branching and merging features over a Subversion working copy. We're also looking forward to sharing in some of the benefits [reported by the Amarok developers](http://amarok.kde.org/blog/archives/1142-A-reflection-How-we-made-Amarok-2.2.1.html) after using it for some time.

Another advantage of [Gitorious](http://www.gitorious.org/) specifically is a lowered barrier to entry for external contributors, thanks to the possibility of making personal clones of the project on the platform and filing a merge request for any changes made.

**Why now?**

Konversation is moving ahead of the bulk of KDE. Here are some reasons why:


+ Due to the fact that most of us have some level of Git experience already, and due to us having a relatively small and stable development team, it's easy for us to make the switch without anyone getting left behind in the process. The goal for the migration of all of KDE is the same, of course, but more work is yet needed to be able to meet it, as detailed [here](http://techbase.kde.org/Projects/MovetoGit). We hope that by migrating early we can increase the momentum behind the entire transition and make ourselves useful in getting it done thanks to the experience garnered.
+ We have some plans, such as making a special version of Konversation for the [Nokia N900](http://maemo.nokia.com/n900/), that are greatly eased by Git's much better support for multi-branch development.


**Where?**

You can find the repository information and spy on our activities on our [Gitorious page](http://www.gitorious.org/konversation). Build instructions are, as always, found on our [wiki](http://konversation.kde.org/wiki/Sources). If you want to jump right in, here's a [clone](http://www.kernel.org/pub/software/scm/git/docs/git-clone.html) command for read-only public access:

`git clone git://gitorious.org/konversation/konversation.git`

Or, if you have a Gitorious account, are a member of [kde-developers](http://www.gitorious.org/+kde-developers) (if not, file a request [here](https://bugs.kde.org/enter_sysadmin_request.cgi)), and want to start working:

`git clone git@gitorious.org:konversation/konversation.git`

**Does this mean you're no longer part of KDE?**
and
**I'm a KDE developer, can I still commit changes to Konversation?**

Konversation without KDE would be unthinkable! :) The project and its repositories on Gitorious are owned by the [kde-developers](http://www.gitorious.org/+kde-developers) team (the same is true for Amarok), which is the official extension of the KDE community into the realm of Gitorious. Anyone with an existing KDE SVN account can very easily ask to be added to the team by [filing a sysadmin request](https://bugs.kde.org/enter_sysadmin_request.cgi) (a more scalable solution for the big KDE move [might be in the cards](http://techbase.kde.org/Projects/MovetoGit#Account_setup_on_Gitorious), though), and anyone in the team can commit to any projects owned by it.

Bottom line: Konversation hasn't parted ways with the KDE community, and never will. The move to Gitorious is done on the assumption that KDE will soon follow, which is the direction things have been going in for a while. However, should the KDE community end up deciding that it would rather host its own Git infrastructure, Konversation will definitely move there.

**I'm a KDE translator doing work on Konversation, what changes for me?**

In short: Nothing! :) Just as with Amarok, Konversation's translations remain in SVN, and KDE's infrastructure (i.e. the scripty bot) takes care of moving data between Git and SVN as needed.

**I want to work on the Konversation handbook, but I can't find it in the Git repository.**

As with the translations, Konversation's documentation [remains in SVN](http://websvn.kde.org/trunk/extragear/network/doc/konversation/) for integration with KDE's [l10n infrastructure](http://l10n.kde.org/).

**I used to get my hands on Konversation by checking out all of extragear, now I need to go to two places. This sucks.**

Yes, this is a bit of a downside at the moment, sorry :(. This has been identified as a problem for the KDE transition to Git some time ago, and work is underway to solve it. The [todo entry](http://techbase.kde.org/Projects/MovetoGit#Script_for_downloading_virtual_KDE_hierarchies) on the KDE Techbase wiki points to a tool meant to be capable of combining SVN and Git projects into virtual sets that can be checked out or updated at once, keeping the established SVN workflow intact. Give it a try, it might work for you already.

**Why am I still seeing you in KDE SVN?**

The directory will be removed shortly (as Amarok's has been), once KDE's [l10n infrastructure](http://l10n.kde.org/) has been updated to look for Konversation at the new place. If you had been planning to commit a change to Konversation, please commit to Git instead!

<hr />

That's it, folks! If you have any more questions, don't hesitate to stop by on our [IRC channel](irc://chat.freenode.net/konversation) or hit our [mailing list](https://mail.kde.org/mailman/listinfo/konversation-devel).
